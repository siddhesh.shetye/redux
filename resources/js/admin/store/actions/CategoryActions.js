import {
    SET_CATEGORY_DEFAULTS,
    HANDLE_CATEGORY_TITLE,
    CREATE_CATEGORIES,
    CREATE_CATEGORIES_SUCCESS,
    CREATE_CATEGORIES_FAILURE,
    SHOW_CATEGORY,
    SHOW_CATEGORY_SUCCESS,
    SHOW_CATEGORY_FAILURE,
    LIST_CATEGORIES,
    LIST_CATEGORIES_SUCCESS,
    LIST_CATEGORIES_FAILURE,
    EDIT_CATEGORIES,
    EDIT_CATEGORIES_SUCCESS,
    EDIT_CATEGORIES_FAILURE,
    DELETE_CATEGORIES,
    DELETE_CATEGORIES_SUCCESS,
    DELETE_CATEGORIES_FAILURE,
} from '../actionTypes/CategoryTypes';

import Category from '../../apis/Category';


/*
    reset to defaults
*/
function setCategoryDefaults() {

    return function(dispatch) {

        dispatch({
            type: SET_CATEGORY_DEFAULTS
        });
    }
}

/*
    Title Change
*/
function handleCategoryTitle(title) {
    return function(dispatch) {

        dispatch({
            type: HANDLE_CATEGORY_TITLE,
            data: title
        })
    }
}

/*
    add category action
*/
function addCategory(title, cb) {

    return function(dispatch) {

        // start creation show spinner
        dispatch({
            type: CREATE_CATEGORIES
        });

        Category.add(title).then(response => {
            dispatch({
                type: CREATE_CATEGORIES_SUCCESS,
                data: response.data
            });
            cb();
        }).catch(error => {
            dispatch({
                type: CREATE_CATEGORIES_FAILURE,
                error: error.response.data
            })
        });
    }
}

/*
    show category action
*/
function showCategory(id) {
    return function(dispatch) {
        // start creation show spinner
        dispatch({
            type: SHOW_CATEGORY
        });

        Category.show(id).then(response => {
            dispatch({
                type: SHOW_CATEGORY_SUCCESS,
                data: response.data
            });

        }).catch(error => {
            dispatch({
                type: SHOW_CATEGORY_FAILURE,
                error: error.response.data
            });
        });
    }
}

/*
    list Categories action
*/
function listCategories() {

    return function(dispatch) {

        // start creation show spinner
        dispatch({
            type: LIST_CATEGORIES
        });

        Category.list().then(response => {
            dispatch({
                type: LIST_CATEGORIES_SUCCESS,
                data: response.data.data
            });
        }).catch(error => {
            dispatch({
                type: LIST_CATEGORIES_FAILURE,
                error: error.response.data
            });
        });
    }
}

/*
    edit category action
*/
function editCategory(title, id) {
    return function(dispatch) {
        // start creation show spinner
        dispatch({
            type: EDIT_CATEGORIES
        });

        Category.edit(title, id).then(response => {
            dispatch({
                type: EDIT_CATEGORIES_SUCCESS,
                data: response.data
            });
        }).catch(error => {
            dispatch({
                type: EDIT_CATEGORIES_FAILURE,
                error: error.response.data
            })
        });
    }
}

/*
    delete category action
*/
function deleteCategory(id) {
    return function(dispatch) {

        // start creation show spinner
        dispatch({
            type: DELETE_CATEGORIES
        });

        Category.remove(id).then(response => {
            dispatch({
                type: DELETE_CATEGORIES_SUCCESS,
                message: response.data.message,
                id: id
            });
        }).catch(error => {
            dispatch({
                type: DELETE_CATEGORIES_FAILURE,
                error: error.response.data
            })
        });
    }
}


export {
    setCategoryDefaults,
    handleCategoryTitle,
    addCategory,
    showCategory,
    listCategories,
    editCategory,
    deleteCategory
};