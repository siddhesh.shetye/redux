import { combineReducers } from 'redux';

import categoryReducer  from './CategoryReducer';
import postReducer from './PostReducer';

const rootReducer = combineReducers({
   category: categoryReducer,
   post: postReducer,
});

export default rootReducer;