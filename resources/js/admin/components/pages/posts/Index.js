import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// actions
import { listPosts, setPostDefaults } from '../../../store/actions/PostActions';

// partials
import Spinner from '../../partials/Spinner';
import SuccessAlert from '../../partials/SuccessAlert';
import ErrorAlert from '../../partials/ErrorAlert';
import Row from './Row';

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchPost: ''
        }
    }

    componentDidMount() {
        this.props.setPostDefaults();

        this.props.listPosts();
    }

    updateSearchFilter(event){
        this.setState({
            searchPost: event.target.value.substr(0, 20)
        });
    }

    render() {
        let filteredPosts = this.props.post.posts;
        
        if(!this.props.post.list_spinner){
            filteredPosts = this.props.post.posts.filter(
                (post) => {
                    return post.title.toLowerCase().indexOf(this.state.searchPost.toLowerCase()) !== -1;
                }
            );
        }

        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Posts
                    </h1>
                </section>

                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">All posts</h3>

                                    <Link to='/posts/add' className="btn btn-primary pull-right">Add <i className="fa fa-plus"></i></Link>
                                </div>

                                <div className="box-body">
                                    <input type="text" value={this.state.searchPost} onChange={this.updateSearchFilter.bind(this)} />
                                </div>

                                <div className="box-body">
                                    <Spinner show={this.props.post.list_spinner} />

                                    <SuccessAlert msg={this.props.post.success_message} />
                                    <ErrorAlert msg={this.props.post.error_message} />

                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Image</th>
                                                <th>Published</th>
                                                <th>Category</th>
                                                <th width="15%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                !this.props.post.list_spinner ? (
                                                    filteredPosts.map(item => <Row key={item.id} post={item} />)
                                                ) : <tr>
                                                        <td> No data </td>
                                                    </tr>
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        post: state.post
    };
};

const mapDispatchToProps = (dispatch) => {

    return {
        setPostDefaults: () => dispatch(setPostDefaults()),
        listPosts: () => dispatch(listPosts()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);