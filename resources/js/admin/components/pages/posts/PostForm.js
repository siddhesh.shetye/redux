import React from 'react';

class Form extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>

                <div className="box-body">
                    <div className={`form-group ${this.props.validation_errors.title ? 'has-error' : ''}`}>
                        <label>Post title</label>
                        <input type="text" className="form-control" placeholder="Post title" onChange={this.props.handleFieldChange} value={this.props.post.title ? this.props.post.title : ''} name="title" />
                        {
                            this.props.validation_errors.title != null ? (<div className="help-block">{this.props.validation_errors.title[0]}</div>) : null
                        }
                    </div>

                    <div className={`form-group ${this.props.validation_errors.content ? 'has-error' : ''}`}>
                        <label>Content</label>
                        <input type="text" className="form-control" placeholder="Post content" onChange={this.props.handleFieldChange} value={this.props.post.content ? this.props.post.content : ''} name="content" />
                        {
                            this.props.validation_errors.content != null ? (<div className="help-block">{this.props.validation_errors.content[0]}</div>) : null
                        }
                    </div>
                </div>

                <div className="box-body">
                    <label>Category</label>
                    <div className={`input-group input-group-sm ${this.props.validation_errors.category_id ? 'has-error' : ''}`}>
                        <select name="category_id" id="category_id" className="form-control" onChange={this.props.handleFieldChange} value={this.props.post.category_id}>
                            <option value="">select category</option>
                            {
                                !this.props.categories.list_spinner ? (

                                    this.props.categories.categories.map(cat => {
                                        return (
                                            <option key={cat.id} value={cat.id}>{cat.title}</option>
                                        )
                                    })

                                ) : ''
                            }
                        </select>
                    </div>
                    {
                        this.props.validation_errors.category_id != null ? (<div className="help-block">{this.props.validation_errors.category_id[0]}</div>) : null
                    }
                </div>

                <div className="box-body">
                    <div className={`form-group ${this.props.validation_errors.image ? 'has-error' : ''}`}>
                        <label>Image</label>
                        <input type="file" name="image" id="image" className="form-control" onChange={this.props.handleFieldChange} accept="image/*" />
                        {
                            this.props.validation_errors.image != null ? (<div className="help-block">{this.props.validation_errors.image[0]}</div>) : null
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default Form;