import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// style
import '../../../css/editor.css';

// partials
import PostForm from './PostForm';

// actions
import { listCategories } from '../../../store/actions/CategoryActions';
import { handleFieldChange, addPost, setPostDefaults, resetFields } from '../../../store/actions/PostActions';


class Add extends React.Component {
    constructor(props) {
        super(props);

        this.handleFieldChange = this.handleFieldChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.setPostDefaults();

        this.props.resetFields();

        this.props.listCategories();
    }

    handleFieldChange(e) {
        if (e.target.name == 'image') {
            this.props.handleFieldChange(e.target.name, e.target.files[0]);
        } else {
            this.props.handleFieldChange(e.target.name, e.target.value);
        }
    }

    handleSubmit(e) {
        e.preventDefault();

        let self = this;

        this.props.addPost(this.props.post.post, function () {

            // reset fields
            self.props.resetFields();

            // redirect
            setTimeout(() => self.props.history.push('/posts'), 2000);
        });
    }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Add Post
                    </h1>
                </section>

                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box box-warning">
                                <div className="box-header with-border">
                                    <h3 className="box-title">Add posts</h3>
                                    <Link to='/posts' className="btn btn-warning btn-sm"><i className="fa fa-arrow-left"></i> Return back </Link>
                                </div>

                                <form method="post" role="form" onSubmit={this.handleSubmit}>

                                    <div className="box-body">
                                        <PostForm post={this.props.post.post}
                                            success_message={this.props.post.success_message}
                                            error_message={this.props.post.error_message}
                                            handleFieldChange={this.handleFieldChange}
                                            categories={this.props.categories}
                                            validation_errors={this.props.post.validation_errors}
                                        />
                                    </div>
                                    <div className="box-footer">
                                        <button type="submit" className="btn btn-success">Submit</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categories: state.category,
        post: state.post
    };
};

const mapDispatchToProps = (dispatch) => {

    return {
        addPost: (payload, cb) => dispatch(addPost(payload, cb)),
        listCategories: () => dispatch(listCategories()),
        handleFieldChange: (field, value, checked = null) => dispatch(handleFieldChange(field, value, checked)),
        setPostDefaults: () => dispatch(setPostDefaults()),
        resetFields: () => dispatch(resetFields())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Add);