import React from 'react';
import { connect } from 'react-redux';
import Row from './Row';
import { Link } from 'react-router-dom';

// actions
import { listCategories, setCategoryDefaults } from '../../../store/actions/CategoryActions';

// partials
import Spinner from '../../partials/Spinner';
import SuccessAlert from '../../partials/SuccessAlert';
import ErrorAlert from '../../partials/ErrorAlert';

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchCategory: ''
        }
    }

    componentDidMount() {
        this.props.setCategoryDefaults();

        this.props.listCategories();
    }

    updateSearchFilter(event) {
        this.setState({
            searchCategory: event.target.value.substr(0, 20)
        });
    }

    render() {
        let filteredCategories = this.props.categories.categories;

        if (!this.props.categories.list_spinner) {
            filteredCategories = this.props.categories.categories.filter(
                (category) => {
                    return category.title.toLowerCase().indexOf(this.state.searchCategory.toLowerCase()) !== -1;
                }
            );
        }

        return (
            <div className="container-fluid page-body-wrapper">
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div className="row">
                            <div className="col-lg-12 grid-margin stretch-card">
                                <div className="card">
                                    <div className="card-body">
                                        <h4 className="card-title">Categories Table</h4>

                                        <div className="form-group row">
                                            <div className="col-sm-10">
                                                <input type="text" className="form-control" placeholder="Search category by title" value={this.state.searchCategory} onChange={this.updateSearchFilter.bind(this)}></input>
                                            </div>
                                            <p className="card-description text-right">
                                                <Link to='/categories/add' className="btn btn-primary"><i className="mdi mdi-open-in-new"></i> New Category</Link>
                                            </p>
                                        </div>

                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        !this.props.categories.list_spinner ? (
                                                            filteredCategories.map(item => <Row key={item.id} category={item} />)
                                                        ) : <tr>
                                                                <td> No data </td>
                                                            </tr>
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.category
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setCategoryDefaults: () => dispatch(setCategoryDefaults()),
        listCategories: () => dispatch(listCategories())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);