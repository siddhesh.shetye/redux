import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// partials
import CategoryForm from './CategoryForm';

// actions
import {
    showCategory, editCategory,
    setCategoryDefaults, handleCategoryTitle
} from '../../../store/actions/CategoryActions';

class Edit extends React.Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.setCategoryDefaults();

        this.props.showCategory(this.props.match.params.id);
    }

    handleChange(e) {
        e.preventDefault();

        this.props.handleTitleChange(e.target.value);
    }

    handleSubmit(e) {
        e.preventDefault();
        let self = this;

        this.props.editCategory(this.props.categories.category.title, this.props.match.params.id, function () {
            // reset title
            self.props.handleTitleChange('');
            
            // redirect
            setTimeout(() => self.props.history.push('/categories'), 2000);
        });
    }

    render() {
        return (
            <div className="container-fluid page-body-wrapper">
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div className="row">
                            <div className="col-12 grid-margin stretch-card">
                                <div className="card">
                                    <div className="card-body">
                                        <h4 className="card-title">Edit Category #{this.props.match.params.id}</h4>
                                        <p className="card-description">
                                            Enter required details
                                    </p>
                                        <form className="forms-sample" role="form" method="post" onSubmit={this.handleSubmit}>
                                            <div className="form-group">
                                                <CategoryForm categories={this.props.categories} onchange={this.handleChange} />
                                            </div>
                                            <button type="submit" className="btn btn-success mr-2">Update</button>
                                            <Link to='/categories' className="btn btn-light"> Cancel </Link>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categories: state.category
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        showCategory: (id) => dispatch(showCategory(id)),
        handleTitleChange: (title) => dispatch(handleCategoryTitle(title)),
        editCategory: (title, id) => dispatch(editCategory(title, id)),
        setCategoryDefaults: () => dispatch(setCategoryDefaults())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);