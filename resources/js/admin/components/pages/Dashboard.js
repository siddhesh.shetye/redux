import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Dashboard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container-fluid page-body-wrapper">
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div className="row">
                            <div className="col-sm-12 flex-column d-flex stretch-card">
                                <div className="row">
                                    <div className="col-lg-4 d-flex grid-margin stretch-card">
                                        <div className="card bg-primary">
                                            <div className="card-body text-white">
                                                <h3 className="font-weight-bold mb-3">Admin Dashboard</h3>
                                                <p className="pb-0 mb-0">Manage activities</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 d-flex grid-margin stretch-card">
                                        <div className="card sale-diffrence-border">
                                            <div className="card-body">
                                                <h2 className="text-dark mb-2 font-weight-bold">Category</h2>
                                                <h4 className="card-title mb-2">Manage </h4>
                                                <Link to="/categories">
                                                    <small className="text-muted">View more</small>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 d-flex grid-margin stretch-card">
                                        <div className="card sale-visit-statistics-border">
                                            <div className="card-body">
                                                <h2 className="text-dark mb-2 font-weight-bold">Posts</h2>
                                                <h4 className="card-title mb-2">Manage </h4>
                                                <Link to="/posts">
                                                    <small className="text-muted">View more</small>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default Dashboard;