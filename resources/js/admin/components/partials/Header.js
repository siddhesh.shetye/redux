import React from 'react';
import { withRouter } from "react-router";
import { Link, NavLink } from "react-router-dom";

class Header extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="horizontal-menu">
                <nav className="navbar top-navbar col-lg-12 col-12 p-0">
                    <div className="container-fluid">
                        <div className="navbar-menu-wrapper d-flex align-items-center justify-content-between">
                            <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                                <a className="navbar-brand brand-logo" href="index.html"><img src="images/favicon.png" alt="logo" /></a>
                                <a className="navbar-brand brand-logo-mini" href="index.html"><img src="images/favicon.png" alt="logo" /></a>
                            </div>
                            <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                                <span className="mdi mdi-menu"></span>
                            </button>
                        </div>
                    </div>
                </nav>
                <nav className="bottom-navbar">
                    <div className="container">
                        <ul className="nav page-navigation">
                            <li className="nav-item">
                                <NavLink exact to='/' className="nav-link">
                                    <i className="mdi mdi-file-document-box menu-icon"></i>
                                    <span className="menu-title">Dashboard</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/categories" className="nav-link">
                                    <i className="mdi mdi-apps menu-icon"></i>
                                    <span className="menu-title">Category</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/posts" className="nav-link">
                                    <i className="mdi mdi-library-books menu-icon"></i>
                                    <span className="menu-title">Posts</span>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

        )
    }
}

export default withRouter(Header)