import axios from "axios";

const Category = {
    add: (title) => {
        return axios.post('/admin/categories', { title });
    },
    show: (id) => {
        return axios.get('/admin/categories/' + id);
    },
    list: () => {
        return axios.get('/admin/categories');
    },
    edit: (title, id) => {
        return axios.put('/admin/categories/' + id, { title });
    },
    remove: (id) => {
        return axios.delete('/admin/categories/' + id);
    },
};

export default Category;