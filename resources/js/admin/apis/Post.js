import axios from "axios";

const Post = {
    list: () => {
        return axios.get('/admin/posts');
    },
    add: (payload) => {
        let data = Post.toFormData(payload);
        return axios.post('/admin/posts', data);
    },
    remove: (id) => {
        return axios.delete('/admin/posts/' + id);
    },

    toFormData: (payload) => {
        const formData = new FormData();

        for (let key in payload) {
            formData.append(key, payload[key]);
        }

        return formData;
    }
};

export default Post;