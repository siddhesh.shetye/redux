import React from 'react';
import {
    HashRouter as Router,
    Link,
    Route,
    Switch
} from 'react-router-dom';
import Dashboard from "./components/pages/Dashboard";
import ListCategories from "./components/pages/categories/Index";
import AddCategories from "./components/pages/categories/Add";
import EditCategories from "./components/pages/categories/Edit";
import ListPosts from "./components/pages/posts/Index";
import AddPosts from "./components/pages/posts/Add";
// import EditPosts from "./components/pages/posts/Edit";

class Routes extends React.Component
{
    render()
    {
        return (
            <Switch>
                <Route exact path='/' component={Dashboard} />
                
                <Route exact path='/categories' component={ListCategories} />
                <Route path='/categories/add' component={AddCategories} />
                <Route path='/categories/edit/:id' component={EditCategories} />
                
                <Route exact path='/posts' component={ListPosts} />
                <Route path='/posts/add' component={AddPosts} />
                {/* <Route path='/posts/edit/:id' component={EditPosts} /> */}
            </Switch>
        )
    }
}

export default Routes;