<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>React Redux Laravel | Dashboard</title>

    <link rel="stylesheet" href="{{ asset('assets/backend/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/backend/css/vendor.bundle.base.css') }}">

    <!-- Google Font -->
    {{-- <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> --}}
</head>

<body>
    <div class="container-scroller">
        <div id="app">
        </div>
    </div>

    <script src="{{ asset('js/admin.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/backend/js/vendor.bundle.base.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('assets/backend/js/template.js') }}" type="text/javascript"></script>
</body>

</html>